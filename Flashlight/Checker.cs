﻿using System;
using System.Linq;


namespace Flashlight
{

    class Checker
    {
        private static readonly int maxCountLightbulbs = 1;
        private static readonly int minCountBatteries = 2;
        private static readonly int maxCountBatteries = 4;
        private Flashlight flashlight = new Flashlight();
        private static readonly int minPowerLightbulb = 1;
        private static readonly int maxPowerLightbulb = 10;
        private static readonly int minPowerBattery = 15;

    
        public Checker (Flashlight flashlight)
        {
            this.flashlight = flashlight;
        }

        public bool CheckInsertionOfBattery(int power)
        {
            if (flashlight.Enabled ==false && power >= minPowerBattery && flashlight.SizeOfListBatteries() <= maxCountBatteries)
            {
                flashlight.InsertBattery(power);
                return true;
            }

            else
            {
                Log.Info("Wrong power of battery or no enough space for battery or flashlight is turned on.");
                return false;
            }

        }

        public bool CheckPullingBattery(int i)
        {

            if (flashlight.GetBatteries() == null && (flashlight.GetBatteries().Any()))
            {
                throw new ArgumentOutOfRangeException("Can't pull battery, because you want to pull battery whick doesn't exist.");

            }

            else if  (flashlight.Enabled == false)
            {
                flashlight.PullBattery(i);
                return true;     
            }

            else
            {
                Log.Info("Can't pull battery, because flashlight is turned on.");
                return false;
            }
            
          
        }


        public bool CheckBattery()
        {
            foreach (Battery aBattery in flashlight.GetBatteries())
            {
                if (aBattery.Power<minPowerBattery)
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckBaterries()
        {
            if (flashlight.SizeOfListBatteries() >= minCountBatteries && flashlight.SizeOfListBatteries() <= maxCountBatteries)
            {
                
                return true;
            }

            else

            {
                Log.Info("Wrong number of batteries.");
                return false;
            }
        }

          public bool CheckInsertionOfLightBulb(int power)
        {
            if (power >= minPowerLightbulb && power <= maxPowerLightbulb && flashlight.GetNumberOfLightbulbs() < maxCountLightbulbs)
            {
                flashlight.InsertLightbulb(power);
                return true;
            }

            else
            {
                Log.Info("Wrong power of lightbulb or no enough space for lightbulb.");
                return false;
            }
        }

        public void CheckChangeLightbulb(int power)
        {
            if (CheckLightbulb() && power >= minPowerLightbulb && power <= maxPowerLightbulb && flashlight.Enabled==false)
            {
                flashlight.ChangeLightbulb(power);
              
            }

            else
            {
                Log.Info("Wrong power of lightbulb or flashlight is turned on.");
                
            }

        }

        public bool CheckLightbulb()
        {
            if (flashlight.GetNumberOfLightbulbs() == maxCountLightbulbs)
            {
                return true;
            }

            else

            {
                Log.Info("Wrong number of lightbulbs.");
                return false;
            }
        }



        public void CheckIfCanBeTurnedOn()
        {
            if (flashlight.Enabled ==false && CheckLightbulb() && CheckBattery() && CheckBaterries())
            {
                flashlight.TurnOn();
                Log.Info();
                Log.Info("Flashlight is turned on.");
            }
            else
            {
                Log.Info();
                Log.Info("Can't turn on flashlight.");
            }
        }

        public void CheckIfCanBeTurnedOff()
        {
            if (flashlight.Enabled == true)
            {
                flashlight.TurnOff();
                Log.Info();
                Log.Info("Flashlight is turned off.");
            }

            else
            {
                Log.Info();
                Log.Info("You can't turn off flashlight, because is turned off.");
            }
        }




    }
}
