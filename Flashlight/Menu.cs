﻿namespace Flashlight
{
    public class Menu
    {

        public static void PrintMenu()
        {
            Log.Info();
            Log.Info("This is menu of a flashlight. You can:");
            Log.Info();
            Log.Info("1. Insert a battery.");
            Log.Info("2. Pull a battery.");
            Log.Info("3. Turn on the flashlight.");
            Log.Info("4. Turn off the flashlight.");
            Log.Info("5. Insert a lightbulb.");
            Log.Info("6. Change a lightbulb.");
            Log.Info("7. Show statistics.");
            Log.Info("8. Exit");
            Log.Info();
            Log.Info("Please choose your action.");

        }

        public static void PrintRequestAboutPowerOfLightbulb()
        {
            Log.Info();
            Log.Info("Please give power of lightbulb:");
            
        }


        public static void PrintRequestAboutPowerOfBattery()
        {
            Log.Info();
            Log.Info("Please give power of battery:");
        }

        public static void PrintRequestAboutIndexOfBattery()
        {
            Log.Info();
            Log.Info("Which battery would you like to pull? (1,2,3,4):");
        }

        public static void PrintStatistics()
        {
            Log.Info();
            Log.Info("Statistics:");
        }

        public static void PrintPowerOfLightbulb()
        {
            Log.Info("Power of lightbulb: ");
        }

        public static void PrintPowerOfBatteries()
        {
            Log.Info("Power of batteries: ");
        }


    }
}
