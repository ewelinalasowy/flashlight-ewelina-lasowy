﻿using System;
using System.Collections.Generic;

namespace Flashlight
{
    class Flashlight
    {
        private static int countLightbulbs = 0;
        private static int countBateries = 0;
        private List<Battery> Batteries = new List<Battery>();
        private Lightbulb lightbulb;

        private bool enabled;

        public Flashlight()
        {
            enabled = false;
       
        }

     
        public bool Enabled
        {
            get
            {
                return enabled;
            }

            set
            {
                enabled = value;

            }
        }

        public void TurnOn()
        {
            enabled=true;
            ReducePower();

        }

        public void TurnOff()
        {
            enabled = false;
        }


        public void InsertBattery(int power)
        {
            Batteries.Add(new Battery(power));
            countBateries++;
            
        }
        
        public void PullBattery(int i)
        {
            Batteries.RemoveAt(i-1);
        }

        public void ShowBatteries()
        {
           foreach (Battery aBattery in Batteries)
            {
                Log.Info(aBattery.Power);
            }

        }

        public List<Battery> GetBatteries()
        {
            return Batteries;
        }

        public int SizeOfListBatteries()
        {
            return countBateries;
        }

        public void ReducePower()
        {
            foreach (Battery aBattery in Batteries)
                aBattery.Power -= lightbulb.Power;
        }

        public void ShowPowerOfLightbulb()
        {
            Log.Info(lightbulb.Power);
        }

        public void InsertLightbulb(int power)
        {
                lightbulb = new Lightbulb(power);
                countLightbulbs++;

        }

        public void ChangeLightbulb(int power)
        {
            lightbulb.Power = power;

        }

        public int GetNumberOfLightbulbs()
        {
            return countLightbulbs;
        }

        
       
    }


}
