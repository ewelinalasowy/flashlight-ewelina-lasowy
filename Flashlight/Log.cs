﻿using System;

namespace Flashlight
{
    public class Log
    {
        public static void Info (String message)
        {
            Console.WriteLine(message);
        }

        public static void Info(int message)
        {
            Console.WriteLine(message);
        }

        public static void Info()
        {
            Console.WriteLine();
        }
    }
}
