﻿namespace Flashlight
{
    class Battery
    {
        
        private int power;

        public int Power
        {
            get
            {
                return power;
            }

            set
            {
                power = value;
                
            }
        }

        public Battery(int power)
        {
            this.power = power;
        }

       
    }
}
