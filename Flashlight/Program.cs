﻿using System;

namespace Flashlight
{
    class Program
    {
        static void Main(string[] args)
        {
           

            Flashlight flashlight = new Flashlight();
            Checker checker = new Checker(flashlight);


            int choose = 0;
            int temp = 0;

            do
            {
                Menu.PrintMenu();
                choose = int.Parse(Console.ReadLine());

                switch (choose)
                {
                    case 1:
                        {
                            Menu.PrintRequestAboutPowerOfBattery();
                            temp = int.Parse(Console.ReadLine());
                            checker.CheckInsertionOfBattery(temp);
                            Log.Info();
                            break;
                        }
                    case 2:
                        {
                            Menu.PrintRequestAboutIndexOfBattery();
                            temp = int.Parse(Console.ReadLine());
                            try
                            {
                                checker.CheckPullingBattery(temp);
                            }
                            catch (ArgumentOutOfRangeException outOfRange)
                            {

                                Log.Info(outOfRange.Message);
                            }
                            Log.Info();
                            break;
                        }
                    case 3:
                        {
                            checker.CheckIfCanBeTurnedOn();
                            Log.Info();
                            break;
                        }
                    case 4:
                        {
                            checker.CheckIfCanBeTurnedOff();
                            Log.Info();
                            break;
                        }
                    case 5:
                        {
                            Menu.PrintRequestAboutPowerOfLightbulb();
                            temp = int.Parse(Console.ReadLine());
                            checker.CheckInsertionOfLightBulb(temp);
                            Log.Info();
                            break;
                        }
                    case 6:
                        {
                            Menu.PrintRequestAboutPowerOfLightbulb();
                            temp = int.Parse(Console.ReadLine());
                            checker.CheckChangeLightbulb(temp);
                            Log.Info();
                            break;
                        }
                    case 7:
                        {
                            Menu.PrintStatistics();
                            Menu.PrintPowerOfLightbulb();
                            try
                            {
                                flashlight.ShowPowerOfLightbulb();
                            }
                            catch (NullReferenceException nullReferenceExcepion)
                            {

                                Log.Info(nullReferenceExcepion.Message);
                            }

                            Menu.PrintPowerOfBatteries();
                            flashlight.ShowBatteries();
                            Log.Info();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }


            } while (choose >=1 && choose <=7);



        }
    }
}
