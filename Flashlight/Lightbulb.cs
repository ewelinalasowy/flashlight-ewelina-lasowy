﻿namespace Flashlight
{
    class Lightbulb
    {
        private int power;

        public int Power
        {
            get
            {
                return power;
            }

            set
            {

                power = value;
                
            }
        }

        public Lightbulb(int power)
        {
            this.power = power;
        }


    }

}
